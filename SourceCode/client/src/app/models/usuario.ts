export interface Usuario {
        id?: number;
        nombre?: string;
        descripcion?: string;
        correo: string;
        contra: string;
        rol?: number;
}
