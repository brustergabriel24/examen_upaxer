-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5977
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table web_upaxer.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table web_upaxer.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `nombre`) VALUES
	(1, 'admin'),
	(2, 'cliente');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table web_upaxer.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `contra` varchar(3000) DEFAULT NULL,
  `rol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_usuario_roles` (`rol`),
  CONSTRAINT `FK_usuario_roles` FOREIGN KEY (`rol`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table web_upaxer.usuario: ~5 rows (approximately)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `nombre`, `descripcion`, `correo`, `contra`, `rol`) VALUES
	(7, 'Gerardo Rosas 2', 'prueba 2', 'bruster_28@outlook.es', '$2b$10$Q9ERgSOW.uVQXTQyTOGI6.pljYACkvZzVXAN3RUPoSJC2pwyxNNpy', 1),
	(8, 'Gerardo Rosas 2', 'prueba 2', 'bruster_29@outlook.es', '$2b$10$UX2/bP0sLYfCCEZjRBSPreRJSO9ZwSCHoy4n416k896PWEufVt8Au', 1),
	(9, 'gerardo', NULL, 'romina.garnelo@gnp.com.mx', '$2b$10$zNM1HwnduNsHd85Hou5sh.p0Zo6gxIeV45aqqls.K0AnppTIyN.Qu', 2),
	(10, 'Gerardo Gabriel', NULL, 'bruster@outlook.es', '$2b$10$vMjVNwmkyjA9piIo6d/lZ.kPCjVp9Y51vyKkWwInRdiVKMZc.VWO.', 2),
	(11, 'gerardo', 'hola prueba de usuario', 'bruster@hotmail.com', '$2b$10$2TDWe40gyyKlTrAR9QcSKO8C8hHbP9WYmMUs1mNgaua0cBnxnuSAO', 2),
	(12, 'gerardosfsdf', '', 'correo', '$2b$10$TtH/SPLtm1nP0mSlvCr1lOcga2CT3B8HXfnh9q0OvuMcZmdJspAw6', 2);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
